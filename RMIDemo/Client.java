import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

    private Client() {}

    public static void main(String[] args) {
        try {
            // Find the registrity
            Registry registry = LocateRegistry.getRegistry();
            MathInterface stub = (MathInterface) registry.lookup("Math");

            // Make a remote method call
            System.out.println("Going to ask the Server to add.");
            int response = stub.add(5, 4);
            System.out.println("response: " + response);
            
            // Make a remote method call
            System.out.println("About to have the Server do subtraction.");
            response = stub.sub(5, 4);
            System.out.println("response: " + response);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}