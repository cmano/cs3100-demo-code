Docs: https://docs.oracle.com/javase/7/docs/technotes/guides/rmi/hello/hello-world.html 

Steps
1) Compile all files

2) Start the rmi registry. Note: This needs to happen from the same directory as the Server class file
    *NIX: $ rmiregistry &
    Windows: start rmiregistry

3) Start the Server
    java Server
    
4) Run the client