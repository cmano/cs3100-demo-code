import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;

public class Server implements MathInterface {

    private Server() {}

    // Define the interface method
    public int add(int x, int y) {
        System.out.println("I'm going to add something.");
        return x + y;
    }

    // Define the interface method
    public int sub(int x, int y) {
        System.out.println("About to do some subtraction.");
        return x - y;
    }

    public static void main(String args[]) {

        try {
            Server obj = new Server();
            MathInterface stub = (MathInterface) UnicastRemoteObject.exportObject(obj, 0);

            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.getRegistry();
            if(Arrays.asList(registry.list()).contains("Math")) { // Remove first if it exists already
                registry.unbind("Math");
            }
            registry.bind("Math", stub);

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}