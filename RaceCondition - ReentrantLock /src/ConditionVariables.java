public class ConditionVariables {
    public static void main(String[] args) {
        BoundedQueue queue = new BoundedQueue(4);

        Producer myProducer = new Producer(queue);
        Consumer myConsumer = new Consumer(queue);

        try {
            Thread t1 = new Thread(myProducer);
            Thread t2 = new Thread(myConsumer);

            t1.start();
            Thread.sleep(1000); // Let the producer fill the queue before starting the consumer
            t2.start();

            t1.join();
            t2.join();

        }
        catch (Exception ex) {
            System.out.println("Something bad happened!");
        }
    }
}
