import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

public class BoundedQueue {
    String[] data;
    private int front = 0;
    private int back = 0;
    private int count = 0;

    private final Lock lock = new ReentrantLock();
    private final Condition isEmpty = lock.newCondition();
    private final Condition isFull = lock.newCondition();

    public BoundedQueue(int size) {
        this.data = new String[size];
    }

    public boolean isEmpty() {
        return data.length == 0;
    }

    public boolean isFull() {
        return data.length == count;
    }

    public void enqueue(String item) throws InterruptedException {
        lock.lock(); // get lock for synchronization

        while (count >= data.length) {
            System.out.println("enqueue: Queue is full, waiting...");
            isFull.await(); // releases lock and waits for signal. Must reacquire lock on signal to return
            System.out.println("...finished waiting, adding to the queue");
        }

        data[back] = item;
        back = (back + 1) % data.length;
        count++;

        isEmpty.signal();   // We know for sure the queue is not empty!

        lock.unlock();
    }

    public String dequeue() throws InterruptedException {
        lock.lock();

        while (count <= 0) {
            System.out.println("dequeue: Queue is empty, waiting...");
            isEmpty.await(); // releases lock and waits for signal. Must reacquire lock on signal to return
            System.out.println("...finished waiting, removing from the queue");
        }

        String item = data[front];
        front = (front + 1) % data.length;
        count--;

        isFull.signal();    // We know it is no longer full!

        lock.unlock();
        return item;
    }
}
