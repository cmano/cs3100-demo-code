
public class Consumer implements Runnable {
    private BoundedQueue queue;

    public Consumer(BoundedQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String item = queue.dequeue();
                System.out.printf("dequeued %s\n", item);
            }
        } catch (Exception ex) {
            System.out.println("Something bad happened in the consumer");
        }
    }
}