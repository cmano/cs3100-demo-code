import java.util.ArrayList;
import java.util.Arrays;

public class Producer implements Runnable {
    private BoundedQueue queue;
    private ArrayList<String> data = new ArrayList<>(Arrays.asList("Paradise", "Millville", "Hyrum", "Nibley", "Logan", "North Logan", "Hyde Park", "Smithfield"));

    public Producer(BoundedQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (!data.isEmpty()) {
                System.out.printf("enqueuing %s\n", data.get(0));
                queue.enqueue(data.remove(0));
            }
        } catch (Exception ex) {
            System.out.printf("Something bad happened in the producer: %s", ex.toString());
        }
    }
}