public class LambdaDemo {
    public static void main(String[] args) {

        //
        // Standard Runnable class implementation
        try {
            WorkerClass w1 = new WorkerClass();
            Thread t1 = new Thread(w1);
            t1.start();
            t1.join();
        }
        catch (Exception ex) {
        }

        //
        // Anonymous class usage, with type inference
        try {
            Runnable w2 = new Runnable() {
                @Override
                public void run() {
                    System.out.println("Anonymous class working...");
                }
            };
            Thread t2 = new Thread(w2);
            t2.start();
            t2.join();
        }
        catch (Exception ex) {
        }

        //
        // Lambda expression usage
        try {
            Runnable w3 = () -> System.out.println("Lambda expression working...");
            Thread t3 = new Thread(w3);
            t3.start();
            t3.join();
        }
        catch (Exception ex) {
        }
    }
}

class WorkerClass implements Runnable {
    @Override
    public void run() {
        System.out.println("Standard class working...");
    }
}
