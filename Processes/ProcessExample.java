import java.io.IOException;
import java.lang.Process;
import java.lang.ProcessBuilder;
import java.util.concurrent.TimeUnit;
import java.io.File;

public class ProcessExample {
    public static void main(String[] args) {

        execProcess();
    }

    public static void execProcess() {
        // Display info about the current directory
        System.out.printf("Current Directory: %s\n", System.getProperty("user.dir"));
        String currentDir = System.getProperty("user.dir");
        File fileDir = new File(currentDir);
        System.out.printf("The parent folder is: %s\n", fileDir.getParent());

        // Change the current directory of the process
        java.nio.file.Path proposed = java.nio.file.Paths.get(currentDir, "test");
        if (proposed.toFile().isDirectory()) { 
            System.out.println("This directory exists!"); 
        } else {
            System.out.println("This directory DOES NOT exit!");
        }
        System.setProperty("user.dir", proposed.toString());
        System.out.printf("Updated Directory: %s\n", System.getProperty("user.dir"));
        System.out.println("Does this make sense?");
        System.out.println();

        // Build a new process
        // String[] command = {"pwd"}; // Try {"cmd", "/c", "cd"} for Windows
        String[] command = {"javac", "ProcessExample.java"};  // Toggle this on and the previous off
        ProcessBuilder pb = new ProcessBuilder(command);
        
        pb.directory(new File(System.getProperty("user.dir"))); // Toggle this
        
        pb.redirectInput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);

        try {
            long start = System.currentTimeMillis();
            Process p = pb.start();

            System.out.println("Starting to wait");
            p.waitFor();
            long end = System.currentTimeMillis();
            System.out.printf("Waited for %d milliseconds\n", end - start);
        }
        catch (IOException ex) {
            System.out.println("Illegal command");
        }
        catch (Exception ex) {
            System.out.println("Something else bad happened");
        }
    }
}

