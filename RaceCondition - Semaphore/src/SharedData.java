import java.util.concurrent.Semaphore;

public class SharedData {
    public volatile long value; // remove volatile with 2 semaphores and notice how errors stop
    public Semaphore lock = new Semaphore(2);

    public SharedData() {
        this.value = 0;
    }

}
