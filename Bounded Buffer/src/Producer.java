public class Producer implements Runnable {
    static int count = 0;
    private SharedBoundedBuffer queue;
    private String name;

    public Producer(SharedBoundedBuffer queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            while (true) {
                queue.empty.acquire();
                queue.lock.acquire();
                int value = count;
                count++;
                queue.enqueue("" + value);
                System.out.printf("Producer %s enqueued %s\n", this.name, "" + value);
                queue.lock.release();
                queue.full.release();
            }
        } catch (Exception ex) {
            System.out.printf("Something bad happened in the producer: %s", ex.toString());
        }
    }
}