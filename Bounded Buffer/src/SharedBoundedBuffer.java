import java.util.concurrent.Semaphore;

public class SharedBoundedBuffer {
    String[] data;
    public Semaphore lock = new Semaphore(1);
    public Semaphore full;
    public Semaphore empty;

    private int front = 0;
    private int back = 0;
    private int count = 0;

    public SharedBoundedBuffer(int size) {
        this.data = new String[size];
        this.full = new Semaphore(size);
        try {
            this.full.acquire(size);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.empty = new Semaphore(size);
    }

    public void enqueue(String item) {
        if (count > this.data.length) {
            System.out.println("Enqueue, but buffer is full");
        }
        data[back] = item;
        back = (back + 1) % data.length;

        count++;
    }

    public String dequeue() {
        if (count < 0) {
            System.out.println("Dequeue, but buffer is empty");
        }
        String item = data[front];
        front = (front + 1) % data.length;

        count--;

        return item;
    }
}
