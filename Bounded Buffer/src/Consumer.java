
public class Consumer implements Runnable {
    private SharedBoundedBuffer queue;
    private String name;

    public Consumer(SharedBoundedBuffer queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            while (true) {
                queue.full.acquire();
                queue.lock.acquire();
                String item = queue.dequeue();
                System.out.printf("Consumer %s dequeued %s\n", this.name, item);
                queue.lock.release();
                queue.empty.release();
            }
        } catch (Exception ex) {
            System.out.println("Something bad happened in the consumer");
        }
    }
}