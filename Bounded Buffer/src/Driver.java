import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        SharedBoundedBuffer queue = new SharedBoundedBuffer(5);
        ArrayList<Producer> producers = new ArrayList<>();
        ArrayList<Consumer> consumers = new ArrayList<>();
        ArrayList<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            producers.add(new Producer(queue, "" + i));
        }
        
        for (int i = 0; i < 4; i++) {
            consumers.add(new Consumer(queue, "" + i));
        }


        try {
            for (Producer i : producers) {
                threads.add(new Thread(i));
            }

            for (Consumer i : consumers) {
                threads.add(new Thread(i));
            }

            for (Thread i : threads) {
                i.start();
            }

            for (Thread i : threads) {
                i.join();
            }

        }
        catch (Exception ex) {
            System.out.println("Something bad happened!");
        }
    }
}
