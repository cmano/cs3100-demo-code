public class AssnXMain {
    public static void main(String[] args) throws Exception {
        OtherFile file1 = new OtherFile();
        AnotherFile file2 = new AnotherFile();
        YetAnotherFile file3 = new YetAnotherFile();

        System.out.println("AssnXMain reporting for duty.");
        System.out.println(file1);
        System.out.println(file2);
        System.out.println(file3);
        System.out.println();

        System.out.print("Here's the input: ");
        for (String i : args) {
            System.out.print(i + " ");
        }
    }
}
