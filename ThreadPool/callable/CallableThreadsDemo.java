import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class CallableThreadsDemo {
    public static void main(String[] args) {
        final long INCREMENT = 1_000_000;
        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() / 4);
        List<Future<Integer>> list = new ArrayList<Future<Integer>>();

        
        try {
            long start = 1;
            long end = INCREMENT;

            long timeStart = System.currentTimeMillis();
            for (int thread = 0; thread < Runtime.getRuntime().availableProcessors(); thread++){
                System.out.println("Adding a thread");
                list.add(threadPool.submit(new CallableComputePrimes(String.format("Thread %d", thread + 1), start, end)));

                start += INCREMENT;
                end += INCREMENT;
            }

            threadPool.shutdown();
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

            for(Future<Integer> fut : list) {
                System.out.println("A Thread found " + fut.get() + " primes");
            }
            long timeEnd = System.currentTimeMillis();
            System.out.printf("The execution time in seconds was: %.3f\n", (timeEnd - timeStart) / 1000.0);
        }
        catch (Exception ex) {
            System.out.println("Something bad happened");
        }
    }
}
