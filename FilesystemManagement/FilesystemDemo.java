import java.io.IOException;
import java.lang.Process;
import java.lang.ProcessBuilder;
import java.util.concurrent.TimeUnit;
import java.io.File;

public class FilesystemDemo {
    
    public static void main(String[] args) {
        filesystemTour();
    }

    public static void filesystemTour() {
        // Get info about the current directory
        System.out.printf("Current Directory: %s\n", System.getProperty("user.dir"));
        String currentDir = System.getProperty("user.dir");
        File fileDir = new File(currentDir);
        System.out.printf("The parent folder is: %s\n", fileDir.getParent());
        System.out.println();

        // Change directories
        java.nio.file.Path proposed = java.nio.file.Paths.get(currentDir, "test");
        if (proposed.toFile().isDirectory()) { 
            System.out.println("This directory exists!"); 
        } else {
            System.out.println("This directory DOES NOT exit!");
        }

        // Get info about the current directory again
        System.setProperty("user.dir", proposed.toString());
        System.out.printf("Updated Directory: %s\n", System.getProperty("user.dir"));
        System.out.println("Does this make sense?");
        System.out.println();
        System.out.println("Can you figure out how to make and remove directories?");
        System.out.println("\tHint is available in the Notes section of the assignment.");
    }
}
