import multiprocessing
import time
from computeprimes import ComputePrimes

print("number of cpus {0:d}".format(multiprocessing.cpu_count()))

INCREMENT = 1000000
threads = []
start = 1
end = INCREMENT
timeStart = time.time()

for thread in range(1, multiprocessing.cpu_count() + 1):
    threads.append(ComputePrimes('{0:d}'.format(thread), start, end))
    threads[thread - 1].start()
    start += INCREMENT
    end += INCREMENT

print("Joining Threads")
for thread in threads:
    thread.join()

print("The execution time in seconds was " + format(time.time() - timeStart, ".5f"))


print("All Done")
