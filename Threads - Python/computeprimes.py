from threading import Thread

class ComputePrimes(Thread):

    def __init__(self, name, startNumber, endNumber):
        Thread.__init__(self)
        self._name = name
        self._startNumber = startNumber
        self._endNumber = endNumber
        self._primes = []

    #
    # Reference: https://www.quora.com/Is-every-prime-number-other-than-2-and-3-of-the-form-6k%C2%B11-Is-this-a-proven-result-What-are-other-resources-about-it
    #
    def isPrime(self, number):
        if number == 2 or number == 3:
            return True
        if number % 2 == 0 or number % 3 == 0:
            return False

        i = 5
        w = 2

        while i * i <= number:
            if number % i == 0:
                return False
            i += w
            w = 6 - w

        return True

    #
    # Report our prime number finding results
    #
    def reportPrimes(self):
        report = 'Thread {0:s} found {1:d} primes'.format(self._name, len(self._primes))
        print(report)

    #
    # Used by Thread to start the thread
    #
    def run(self):
        current = self._startNumber

        while current <= self._endNumber:
            if self.isPrime(current):
                self._primes.append(current)
            current += 1

        self.reportPrimes()

    def getName(self):
        return self._name

