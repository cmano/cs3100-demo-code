#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int withExec() {
    pid_t pid; // Create the process id variable
    char word[] = "Pineapple";
    pid = fork(); // Create a new process

    if (pid < 0) { // Check for an error
        fprintf(stderr, "Fork Failed");
        return 1;
    }
    else if (pid == 0) { // Child process
        printf("I'm the child\n");
        printf("The word is %s\n", word); // Shows copied variable
        execlp("/bin/ls", "ls", NULL);
        printf("I disappeared!!!!\n"); // This never happens. Why?
    }
    else { // Parent process
        // wait(NULL); //toggle this on and off
        printf("I'm the parent\n");
    }

    return 0;
}

int withStatus() {
    pid_t pid; // Create the process id variable
    int status;
    pid = fork(); // Create a new process
            
    if (pid < 0) { // Check for an error
        fprintf(stderr, "Fork Failed");
        return 1;
    }
    else if (pid == 0) { // Child process
        printf("I'm the child\n");
        exit(10); // return 10; also works
    }
    else { // Parent process
        pid_t x = wait(&status);
        printf("%u\n", WEXITSTATUS(status)); // Use a macro on status to get just the returned int
    }

    return 0;
}

int main() {
    // Just call one of these at a time.
    withExec();
    // withStatus(); 
}