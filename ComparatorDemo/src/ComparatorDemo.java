import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Iterator;

public class ComparatorDemo {

    public static void main(String[] args) {

        PriorityQueue<Thing> queue = new PriorityQueue<Thing>(1, new CompareThings());

        // ******Iterator (illustrate the ordering)******* //
        System.out.println("Iterator Usage");
        resetQueue(queue);
        System.out.printf("Length: %d\n", queue.size());

        Iterator<Thing> iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.printf("Something: %d\n", iterator.next().value);
            iterator.remove();
        }

        System.out.printf("Length: %d\n\n", queue.size());

        // ******Peek and Remove******* //
        System.out.println("Peek and Remove");
        resetQueue(queue);
        System.out.printf("Length: %d\n", queue.size());

        Thing nextThing = queue.peek(); // Get the next item, but don't remove. Null if empty
    
        while (nextThing != null) {
            System.out.printf("Something: %d\n", nextThing.value);
            queue.remove(nextThing); // Remember to remove it from the queue after using it
            nextThing = queue.peek(); // Peek at the next item
        }

        System.out.printf("Length: %d\n\n", queue.size());

        // ******Poll******* //
        System.out.println("Poll");
        resetQueue(queue);
        System.out.printf("Length: %d\n", queue.size());

        Thing currentThing = queue.poll(); // Get the next item, and remove from queue. Null if empty
    
        while (currentThing != null) {
            System.out.printf("Something: %d\n", currentThing.value);
            currentThing = queue.poll(); // Poll the next item
        }

        System.out.printf("Length: %d\n\n", queue.size());
    }

    // Load up the queue with a method to make things clean
    private static void resetQueue(PriorityQueue<Thing> queue) {
        queue.add(new Thing(65));
        queue.add(new Thing(100));
        queue.add(new Thing(26));
        queue.add(new Thing(104));
    }
}

class CompareThings implements Comparator<Thing> {
    // Override the compare method to define what it means for Things
    // to be less than, greater than, or equal to
    public int compare(Thing t1, Thing t2) {
        if (t1.value < t2.value) { // Things 1 is less than Thing 2
            return -1;
        }
        else if (t1.value > t2.value) { // Things 1 is greater than Thing 2
            return 1;
        }
        // Thing 1 is equal to Thing 2
        return 0;
    }
}
