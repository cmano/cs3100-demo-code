// Compile with g++ PThreadsDemo.cpp

#include <iostream>
#include <pthread.h> // using this for threads
#include <vector>
#include <thread> // use this because getting cpus is easy

struct info {
    int name;
    long start;
    long end;
};

bool isPrime(long number)
{
	if (number == 2 || number == 3) return true;
	if (number % 2 == 0 || number % 3 == 0) return false;

	long i = 5;
	long w = 2;

	while (i * i <= number) {
		if (number % i == 0) {
			return false;
		}
		i += w;
		w = 6 - w;
	}

	return true;
}

void reportPrimes(int name, std::vector<long>& primes)
{
	std::cout << "Thread " << name << " found " << primes.size() << " primes" << std::endl;
}

void *computePrimes(void* params)
{
    struct info* arg = (struct info*)params;
	long current = arg->start;
	std::vector<long> primes;

	while (current <= arg->end)
	{
		if (isPrime(current))
		{
			primes.push_back(current);
		}
		current++;
	}

	reportPrimes(arg->name, primes);
    return (void*)NULL;
}

int main() {
	const long INCREMENT = 1000000;
	long start = 1;
	long end = INCREMENT;
    int cpus = std::thread::hardware_concurrency();
    pthread_t threads[cpus];

	std::chrono::time_point<std::chrono::high_resolution_clock> timeStart = std::chrono::high_resolution_clock::now();

	for (unsigned int threadID = 0; threadID < cpus; threadID++) //number of concurrent threads supported
	{
        info *tmp = new info;

        tmp->name = threadID + 1;
        tmp->start = start;
        tmp->end = end;

        if (pthread_create(&(threads[threadID]), NULL, computePrimes, tmp)) {
            perror("pthread_create() failed");
            exit(EXIT_FAILURE);
        }
        else {
            std::cout << "Spawned thread #" << threadID << std::endl;
        } 
		start += INCREMENT;
		end += INCREMENT;
	}

    for (int i = 0; i < cpus; ++i) {
        pthread_join(threads[i], NULL);
    }

	std::chrono::time_point<std::chrono::high_resolution_clock> timeEnd = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsedTime = timeEnd - timeStart;
	std::cout << "The execution time in seconds was: " << elapsedTime.count() << std::endl;

	return 0;
}
