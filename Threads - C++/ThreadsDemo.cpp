// Uses C++ 11 thread library
// Compile with: $ clang++ -std=c++11 -stdlib=libc++ ThreadsDemo.cpp

#include <iostream>
#include <vector>
#include <thread>

bool isPrime(long number)
{
	if (number == 2 || number == 3) return true;
	if (number % 2 == 0 || number % 3 == 0) return false;

	long i = 5;
	long w = 2;

	while (i * i <= number) {
		if (number % i == 0) {
			return false;
		}
		i += w;
		w = 6 - w;
	}

	return true;
}

void reportPrimes(int name, std::vector<long>& primes)
{
	std::cout << "Thread " << name << " found " << primes.size() << " primes" << std::endl;
}

void computePrimes(int name, long start, long end)
{
	long current = start;
	std::vector<long> primes;

	while (current <= end)
	{
		if (isPrime(current))
		{
			primes.push_back(current);
		}
		current++;
	}

	reportPrimes(name, primes);
}

int main()
{
	const long INCREMENT = 1000000;
	long start = 1;
	long end = INCREMENT;

	std::chrono::time_point<std::chrono::high_resolution_clock> timeStart = std::chrono::high_resolution_clock::now();

	std::vector<std::thread*> threads;

	for (unsigned int thread = 0; thread < std::thread::hardware_concurrency(); thread++) //number of concurrent threads supported
	{
		threads.push_back(new std::thread(computePrimes, thread + 1, start, end));

		start += INCREMENT;
		end += INCREMENT;
	}

	for (std::thread* t : threads)
	{
		t->join();
	}

	std::chrono::time_point<std::chrono::high_resolution_clock> timeEnd = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsedTime = timeEnd - timeStart;
	std::cout << "The execution time in seconds was: " << elapsedTime.count() << std::endl;

	return 0;
}
