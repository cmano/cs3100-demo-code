import java.util.Random;

public class Consumer implements Runnable {
    private SharedData data;
    private Random random;

    public Consumer(SharedData data) {
        this.data = data;
        this.random = new Random();
    }

    @Override
    public void run() {

        while (true) {
            if (this.random.nextInt(2) == 0) {
                this.data.decrement();
            } else {
                this.data.subtract();
            }
        }
    }

}
