public class SharedData {
    public volatile long value;
    public volatile long number;
    private Object monitor1;
    private Object monitor2;

    public SharedData() {
        this.value = 0;
        this.number = 0;
        this.monitor1 = new Object();
        this.monitor2 = new Object();
    }

    public void increment() {
        synchronized(monitor1) {
            long before = this.value;
            this.value++;
            long after = this.value;

            if (before != (after - 1)) {
                System.out.println("Increment - Problem detected!!");
                System.out.printf("Expected : %d\n", before + 1);
                System.out.printf("Got      : %d\n", after);
            }
        }
    }

    public void decrement() {
        synchronized(monitor1) {
            long before = this.value;
            this.value--;
            long after = this.value;

            if (after != (before - 1)) {
                System.out.println("Decrement - Problem detected!!");
                System.out.printf("Expected : %d\n", before + 1);
                System.out.printf("Got      : %d\n", after);
            }
        }
    }

    public void add() {
        synchronized(monitor2) {
            long before = this.number;
            this.number++;
            long after = this.number;

            if (before != (after - 1)) {
                System.out.println("Add - Problem detected!!");
                System.out.printf("Expected : %d\n", before + 1);
                System.out.printf("Got      : %d\n", after);
            }
        }
    }

    public void subtract() {
        synchronized(monitor2) {
            long before = this.number;
            this.number--;
            long after = this.number;

            if (after != (before - 1)) {
                System.out.println("Subtract - Problem detected!!");
                System.out.printf("Expected : %d\n", before + 1);
                System.out.printf("Got      : %d\n", after);
            }
        }
    }
}
