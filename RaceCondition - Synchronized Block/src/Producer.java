import java.util.Random;

public class Producer implements Runnable {
    private SharedData data;
    private Random random;

    public Producer(SharedData data) {
        this.data = data;
        this.random = new Random();
    }

    @Override
    public void run() {

        while (true) {
            if (this.random.nextInt(2) == 0) {
                this.data.increment();
            } else {
                this.data.add();
            }
        }
    }

}
