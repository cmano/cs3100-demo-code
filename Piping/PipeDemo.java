public class PipeDemo {

        public static void main(String[] args) {
                String[] p1Cmd = { "ls", "-l", "-a"};
                String[] p2Cmd = { "grep", "1318" }; // Change the number to something in the process list output

                ProcessBuilder pb1 = new ProcessBuilder(p1Cmd);
                ProcessBuilder pb2 = new ProcessBuilder(p2Cmd);

                // Use the parent process's I/O channels
                pb1.redirectInput(ProcessBuilder.Redirect.INHERIT);
                pb2.redirectOutput(ProcessBuilder.Redirect.INHERIT);

                try {
                        Process p1 = pb1.start();
                        Process p2 = pb2.start();

                        // this might feel backwards, but it is our program's input and output
                        // thus p1's output is our input and our output is p2's input
                        java.io.InputStream in = p1.getInputStream();
                        java.io.OutputStream out = p2.getOutputStream();

                        // Read the data from p1 and feed to p2.
                        int data;
                        while ((data = in.read()) != -1) {
                                out.write(data);
                        }

                        // make sure all data is sent to p2 and let p2 know we're done.
                        out.flush();
                        out.close();

                        // Java version of wait()
                        p1.waitFor();
                        p2.waitFor();
                }
                catch (Exception ex) {
                }
        }

}
